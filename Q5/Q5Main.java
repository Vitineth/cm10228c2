public class Q5Main {

    public static void main(String[] args) {
        KeyValuePairList<String, String> stringPairs = new KeyValuePairList<>();
        System.out.println(stringPairs.toString());
        // Outputs 10 null elements as the array is initialised

        System.out.println("-- Adding ");
        stringPairs.add("Key", "value");
        System.out.println(stringPairs.toString());
        // Outputs 1 element and 9 null elements

        System.out.println("-- Adding 10");
        for (int i = 0; i < 10; i++) {
            stringPairs.add("key" + i, "value" + i);
        }
        System.out.println(stringPairs.toString());
        // Outputs 11 filled elements and 9 null elements as the array is resized to cope

        System.out.println("-- Removing");
        stringPairs.remove("key7");
        System.out.println(stringPairs.toString());
        // Outputs 10 filled elements and 10 null elements

        System.out.println("-- Finding");
        System.out.println(stringPairs.find("key8"));
        System.out.println(stringPairs.find("key7"));
        // Outputs value8 and null respectively

        System.out.println("-- Removing");
        stringPairs.remove("key8");
        System.out.println(stringPairs.toString());
        // Outputs 9 filled elements and 1 null element as the array is resized to cope
    }

}

@SuppressWarnings("unchecked")
class KeyValuePairList<K, V> {

    /**
     * Stores the initial size of the array that will back this list store. This is used in the
     * {@link #KeyValuePairList()} constructor. This is only declared here to avoid magic numbers even though IntelliJ
     * complains.
     */
    private final int INITIAL_SIZE = 10;

    /**
     * Using the type specified as the key format to form an array that will store all of the keys currently contained
     * within this set
     */
    private K[] keys;
    /**
     * Using the type specified as the value format to form an array that will store all of the values currently
     * contained within this set
     */
    private V[] values;
    /**
     * Stores the current number of elements that are actually contained within this set
     */
    private int size;
    /**
     * Stores the location of the next free space within the set
     */
    private int currentFreePosition = 0;

    /**
     * Constructs a new KeyValuePairList which will begins with a key and a value array with an initial size of
     * {@link #INITIAL_SIZE}.
     */
    public KeyValuePairList() {
        keys = (K[]) new Object[INITIAL_SIZE];
        values = (V[]) new Object[INITIAL_SIZE];
    }

    /**
     * Adds a new key to the set.
     * This will use the next available location in teh array to store the value as indicated by the
     * {@link #currentFreePosition} value. If the next free location is the size of the array, meaning that there
     * is no space left, a new set of arrays, twice the length of hte existing array will be created, the values will
     * be copied across and and these arrays will replace the existing arrays.
     * @param key The new key to add to the array
     * @param value The new value to add to the array
     */
    public void add(K key, V value) {
        if (currentFreePosition == keys.length) {
            K[] newKeys = (K[]) new Object[keys.length * 2];
            V[] newValues = (V[]) new Object[values.length * 2];
            System.arraycopy(keys, 0, newKeys, 0, keys.length);
            System.arraycopy(values, 0, newValues, 0, values.length);
            keys = newKeys;
            values = newValues;
        }

        keys[currentFreePosition] = key;
        values[currentFreePosition] = value;
        currentFreePosition++;
        size++;
    }

    /**
     * This will remove all of the values with the given key from the set. It will identify the last filled position
     * in the array and use its location to replace the element in the array that needed to be removed with it. This
     * means that the array size can be reused.
     * If the array size of the array is double that of the amount of elements within it, a new key and value store with
     * half the size of the existing stores are created, the elements in the existing arrays are copied across and these
     * new arrays will replace the existing key and value arrays.
     * @param key THe key to remove
     */
    public void remove(K key) {
        for (int i = 0; i < keys.length; i++) {
            if (keys[i] != null && keys[i].equals(key)) {
                int lastFilledPosition = currentFreePosition;
                while (keys[lastFilledPosition] == null) lastFilledPosition--;
                keys[i] = keys[lastFilledPosition];
                values[i] = values[lastFilledPosition];
                keys[lastFilledPosition] = null;
                values[lastFilledPosition] = null;
                currentFreePosition = lastFilledPosition;
                size--;
            }
        }

        if (size < keys.length / 2) {
            K[] newKeys = (K[]) new Object[keys.length / 2];
            V[] newValues = (V[]) new Object[values.length / 2];
            System.arraycopy(keys, 0, newKeys, 0, newKeys.length);
            System.arraycopy(values, 0, newValues, 0, newValues.length);
            keys = newKeys;
            values = newValues;
        }
    }

    /**
     * Finds the first matching element in the set with the given key
     * @param key The key to search for
     * @return The first value with the given key
     */
    public V find(K key) {
        for (int i = 0; i < keys.length; i++) {
            if (keys[i] != null && keys[i].equals(key)) {
                return values[i];
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keys.length; i++) {
            sb.append("\n").append(i).append(": [\"").append(keys[i]).append("\" : \"").append(values[i]).append("\"]");
        }
        return sb.substring(1);
    }
}
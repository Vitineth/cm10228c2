#include <stdio.h>
#include <malloc.h>
#include <mem.h>

// This is a copy of the compareTo function in java as described in q2.c.

int compare(char *name1, char *name2) {
    int length = strlen(name1);
    if (strlen(name2) < length) length = strlen(name2);

    int compare = 0;
    for (int k = 0; k < length; ++k) {
        compare = name2[k] - name1[k];
        if (compare != 0) break;
    }

    if (compare == 0) compare = strlen(name2) - strlen(name1);
    return compare;
}

int partition(char *names[], int low, int high) {
    char *pivot = names[high];
    int i = low - 1;
    for (int j = low; j < high; ++j) {
        if (compare(names[j], pivot) > 0) {
            i++;
            char *temp = names[j];
            names[j] = names[i];
            names[i] = temp;
        }
    }

    char *temp = names[i + 1];
    names[i + 1] = names[high];
    names[high] = temp;

    return i+1;
}

// So we can pass an array to a function without having to do any magic to it because when we
// call it, C automatically converts the array into a pointer, also known as decaying the pointer.
// This means because we are now accessing a pointer, we do not need to go on and fetch the address
// and we will be actually editing the original array, not just a copy. All the sort code is the
// same as the java implementation copied from Wikipedia originally so I'm not going into detail.

void sort(char *names[], int low, int high) {
    if (low < high) {
        int p = partition(names, low, high);
        sort(names, low, p - 1);
        sort(names, p + 1, high);
    }
}

int main(int argc, char *argv[]) {
    // argv 1 - name of file to read in a list of names
    // argv 2 - name of file to write out the sorted list of names
    if (argc == 2) {
        printf("%s\n", argv[1]);
    } else if (argc == 3) {
        printf("%s\n", argv[1]);
        printf("%s\n", argv[2]);
    }

    // Reading from the file. We need to open the file which we do by creating a file pointer to the path specified in
    // the command line parameters. We open it in read mode.
    FILE *fp = fopen(argv[1], "r");

    // If we want to store all of the names that are contained within the file, we need to define an array to hold them
    // but because arrays are of a fixed size, we need to count them first. Therefore we scan through the file and
    // count up the number of commas which are our delimiters.
    int c = 0;
    int read;
    while ((read = fgetc(fp)) != EOF) {
        if (read == ',') {
            c++;
        }
    }
    // Add one for the last entry in the file
    c++;

    // Now that we know the size, we can define the array to hold them
    char *words[c];

    //Next we want to read in the file so that we can do some processing on it. To do that we need to know how many
    // characters we need to read and to do that we need to know how long the file is.
    // As we know that we are at the end of the file if we reach this point because the loop above
    // will only drop out on EOF, we can just grab our current position and reset the file index to the
    // beginning of the file so we are ready to read again.
    // ftell should always be positive at this point unless something is very wrong. This could
    // be a dangerous cast but we will see. If ftell does return a negative value then this will cause
    // some crazy behaviour and likely cause a massive amount of memory to be allocated by malloc below.
    unsigned long size = (unsigned long) ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // Then we define the memory for the file and read in the contents of the file. We use a size of one byte and read
    // size number of elements from the fp pointer.
    char *fcontent = malloc(size);
    fread(fcontent, 1, size, fp);

    // Then we close the file so we don't have handle issues later.
    fclose(fp);

    // This bit of the code is a little bit of a mess
    // We define an int to act as the index we are currently writing to in the words array, effectively the current name
    // we are on
    int i = 0;
    // We use strtok to take the first set of characters all the way up to the first , character. This will get us the
    // first name.
    char *token = strtok(fcontent, ",");
    // As the name is formatted as "NAME", we need to remove the first and last character of the string so we do this
    // by incrementing the start pointer which means the start points to one character in and set the last character to
    // a null terminator which seems to do the job
    token++;
    token[strlen(token) - 1] = '\0';
    // Then we store the name and increment the index after that is done
    words[i++] = token;

    // Next we find the next token in preparation for the next loop. We use NULL as the string here as it will cause it
    // to continue searching the string that we first specified from the end of the previous token but we use the same
    // delimiter character.
    token = strtok(NULL, ",");
    // While we have a valid token (this will be NULL when we reach the end of the file as the strtok function returns
    // NULL if there are no more valid tokens contained within the string.)
    while (token != NULL) {
        // Remove the first and last character as described above
        token++;
        token[strlen(token) - 1] = '\0';
        // Store the word in the same way described above
        words[i++] = token;
        // And find the next token
        token = strtok(NULL, ",");
    }

//    Sorting code here
    sort(words, 0, (sizeof(words) / sizeof(words[0])) - 1);

    // To produce the output in the same format the file was in, we need to create a string that contains every name
    // wrapped in quotes, delimited by commas. We know that the content of the names will not be changed and we know
    // that we will be readding the quotes and commas that we removed earlier so it will be the same length as the
    // original so we just need to define a piece of memory that large.
    char *output = malloc(size);
    // We need to know the position in the memory that we are currently writing to so we create an index for that
    int outputCounter = 0;

    // Then for each word in the list of names (magic described above)
    for (int l = 0; l < sizeof(words) / sizeof(words[0]); ++l) {
        // All writes to the output will use the index outputCounter++ which will use the current value of outputCounter
        // but will increment it to the next value afterwards. This means that it will always point to the next
        // available space when the line completes.

        // If this isn't the first value, we add a comma. This saves us having to remove one character at the end
        if (outputCounter != 0) output[outputCounter++] = ',';
        // Then we add the first quote
        output[outputCounter++] = '\"';
        // Copy in the word character by character (there is likely a better way to do this)
        for (int j = 0; j < strlen(words[l]); ++j) {
            output[outputCounter++] = words[l][j];
        }
        // And wrap it in the other quote
        output[outputCounter++] = '\"';
    }
    // Then we finally add a null terminator to make sure the string ends
    output[outputCounter] = '\0';

    // If we gave two parameters, an infile and an outfile, we open the outfile, write the output and close it
    if (argc == 3) {
        FILE *outputFile = fopen(argv[2], "w");
        fputs(output, outputFile);
        fclose(outputFile);
    } else {
        // If we did not, then we just dump the response onto the standard output
        printf("%s\n", output);
    }

    // And we exit successfully.
    return 0;
}


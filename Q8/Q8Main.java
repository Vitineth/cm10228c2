import java.io.*;

@SuppressWarnings("Duplicates")
public class Q8Main {

    public static void main(String[] args) {
        // args 0 - name of file to read in text
        String inputFileName = null;
        // args 1 - name of file to read in list of words to redact
        String toRedact = null;
        // args 2 - name of file to write out the redacted text
        String outputFileName = null;

        if (args.length == 2) {
            inputFileName = args[0];
            toRedact = args[1];
        } else if (args.length == 3) {
            inputFileName = args[0];
            toRedact = args[1];
            outputFileName = args[2];
        }

        // call method to redact words from input text
        new Q8Main().redactText(inputFileName, toRedact, outputFileName);
    }

    public void redactText(String input, String toRedact, String output) {
        File inputFile = new File(input);
        File toRedactFile = new File(toRedact);
        File outputFile = null;
        if (output != null) {
            outputFile = new File(output);
            // Verify the output file and output a warning if it exists
            if (outputFile.exists()) System.err.println("Output file exists, it will be overwritten");
        }

        // Verify the input file exists
        if (!inputFile.exists()) {
            // Otherwise throw a descriptive error and exit
            try {
                throw new FileNotFoundException("Input file not found");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }

        // Verify the redaction file exists
        if (!toRedactFile.exists()) {
            // Otherwise throw a descriptive error and exit
            try {
                throw new FileNotFoundException("Input file not found");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }

        // Read in all the content from the file
        String inputRaw;

        try {
            FileInputStream fis = new FileInputStream(inputFile);
            byte[] buffer = new byte[1024];
            int len;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = fis.read(buffer)) != -1) baos.write(buffer, 0, len);

            inputRaw = baos.toString();
        } catch (IOException e) {
            System.err.println("Failed to read the input file");
            e.printStackTrace();
            return;
        }
        // Read in all the content from the file
        String redactionRaw;

        try {
            FileInputStream fis = new FileInputStream(toRedactFile);
            byte[] buffer = new byte[1024];
            int len;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = fis.read(buffer)) != -1) baos.write(buffer, 0, len);

            redactionRaw = baos.toString();
        } catch (IOException e) {
            System.err.println("Failed to read the input file");
            e.printStackTrace();
            return;
        }

        // Get the actual phrases that we must remove
        String[] redactionPhrases = redactionRaw.split("\n");
        // Convert the input text to a character array
        char[] characters = inputRaw.toCharArray();

        // I'm using three flags for this. If we could be redacting a word, activeRedaction would be true which will
        // happen when a word starts with a capital letter.
        // If there is a full stop before the word then pauseRedaction will be true which states that we should skip
        // this word
        // If we have paused redaction, we need to find out when we should activate it again. This will happen when
        // we have hit whitespace, after we have hit the characters of the word. Therefore this will be false initially
        // when pause redaction is enabled and will flip to true when we hit a proper character
        boolean pauseRedaction = false;
        boolean activeRedaction = false;
        boolean charactersHit = false;

        // Then for every character
        for (int i = 0; i < characters.length; i++) {
            // If it is a full stop or we have had at least two new lines (a new paragraph or chapters), we want to
            // pause the redaction and we need to set characters hit to false so that we can keep track of when we need
            // to enable it again
            if (characters[i] == '.' || (characters[i] == '\n' && characters[i - 1] == '\n')) {
                pauseRedaction = true;
                charactersHit = false;
            }
            // Then if we hit an actual letter we set that we have hit the character
            if (Character.isAlphabetic(characters[i])) charactersHit = true;
            // If we are currently pausing redaction but hit a whitespace character, and we have already reached the
            // characters of word that we should be skipping, we stop the pause
            if (pauseRedaction && charactersHit && Character.isWhitespace(characters[i])) pauseRedaction = false;

            // If we reach an upper case character then we could potentially be meant to be redacting
            if (Character.isUpperCase(characters[i])) activeRedaction = true;
            // And if we hit whitespace we should stop
            if (Character.isWhitespace(characters[i])) activeRedaction = false;

            // Then, finally, if we should potentially be redacting and we shouldn't be skipping this word because it
            // was the first word of a sentence or paragraph then we should replace the current character with a star
            // to redact it
            if (activeRedaction && !pauseRedaction) characters[i] = '*';
        }

        // Then we convert the array back to a string for the next set of replacements
        String outputData = new String(characters);

        // This bit is very likely cheating but we get the redaction we are meant to be making, trim off the whitespace
        // in case there is any on either end, create a string of stars equal to the length and then replace all
        // instances of the redaction word (not sure if you're allowed to use .replace(target, replacement)
        for (String redaction : redactionPhrases) {
            redaction = redaction.trim();

            String redactionStars = "";
            for (int i = 0; i < redaction.length(); i++) redactionStars += "*";
            outputData = outputData.replace(redaction, redactionStars);
        }

        // If we were given an output file then we open it, write the output, flush the data and close it.
        if (outputFile != null) {
            try {
                FileOutputStream fos = new FileOutputStream(outputFile);
                fos.write(outputData.getBytes());
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // Otherwise we dump it to the console
            System.out.println(outputData);
        }
    }
}
import java.io.*;

@SuppressWarnings("Duplicates")
public class Q9Main {

    public static void main(String[] args) {
        // args 0 - name of file to read in the text to encrypt
        String inputFileName = null;
        // args 1 - name of file to write out the encrypted text
        String outputFileName = null;
        if (args.length == 1) {
            inputFileName = args[0];
        } else if (args.length == 2) {
            inputFileName = args[0];
            outputFileName = args[1];
        }

        // call method to encrypt text
        new Q9Main().encrypteText(inputFileName, outputFileName);
    }

    public void encrypteText(String input, String output) {
        File inputFile = new File(input);
        File outputFile = null;

        if (output != null) {
            outputFile = new File(output);
            // Verify the output file and output a warning if it exists
            if (outputFile.exists()) System.err.println("Output file exists, it will be overwritten");
        }

        // Verify the input file exists
        if (!inputFile.exists()) {
            // Otherwise throw a descriptive error and exit
            try {
                throw new FileNotFoundException("Input file not found");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }

        // Read in all the content from the file
        String inputRaw;

        try {
            FileInputStream fis = new FileInputStream(inputFile);
            byte[] buffer = new byte[1024];
            int len;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = fis.read(buffer)) != -1) baos.write(buffer, 0, len);

            inputRaw = baos.toString();
        } catch (IOException e) {
            System.err.println("Failed to read the input file");
            e.printStackTrace();
            return;
        }

        // We need to form the vignere square. We can do this pretty easily as it turns out.
        // We create a 26x26 grid (like the one shown in the document)
        // We need a list of all the characters so that we can iterate through them. Then, for each row in the
        // grid (y), we need to offset the start of the character list. For example, the first line starts with A which
        // is at index 0 and then second line starts with B which is at index 1 and so on.
        // Then, we go across each column (x) and we set it to the current position of the pointer in the string. Then
        // we increment the pointer to point at the next character. If it is over the length of the string of characters
        // then we need to point it back to the start.
        // This successfully creates the grid
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int pointer = 0;
        char[][] vignereSquare = new char[26][26];
        for (int y = 0; y < vignereSquare.length; y++) {
            pointer = y;
            for (int x = 0; x < vignereSquare[0].length; x++) {
                vignereSquare[y][x] = alpha.charAt(pointer);
                if (++pointer >= alpha.length()) pointer = 0;
            }
        }

        // Define the key that we were given
        String key = "LOVELACE";
        // And reset the pointer to 0
        pointer = 0;
        // Then for the length of the input, we need to add that many characters to the key to ensure that we have
        // enough. This should lead to an overestimate as it will include whitespace and characters that we are ignoring
        // We use the same pointer system that I used before here so when it reaches the end of the key, it loops back
        // to the start
        for (int i = 0; i < inputRaw.length(); i++) {
            key += key.charAt(pointer++);
            if (pointer >= key.length()) pointer = 0;
        }

        // Then we reset the pointer once again and this will be used to point the current character in the key
        pointer = 0;
        // We convert the string to character array so we can do in place character changes
        char[] inputChars = inputRaw.toCharArray();
        // Then for each character in the message
        for (int i = 0; i < inputChars.length; i++) {
            // If the character is a letter (upper case or lower case)
            if (Character.isAlphabetic(inputChars[i])) {
                // Then we convert the character to upper case and subtract 65. This is the ASCII code of A so it will
                // mean that A refers to 0 and so on. This allows us to lookup stuff in the grid
                int inputIndex = Character.toUpperCase(inputChars[i]) - 65;
                // Then we do the same with the key with our current pointer to where we are in the key and once we
                // have accessed that, we increment the pointer
                int keyIndex = Character.toUpperCase(key.charAt(pointer++)) - 65;

                // Then we look up the new character in the square based on the merging of the key and message
                char newChar = vignereSquare[inputIndex][keyIndex];
                // Then we need to replace the character. There are two options of this line, this one will replace it
                // with the correct case of character. To do that we check if the initial input if upper case, if it
                // is we use the new character we got, otherwise we convert the new character to lower case and use that
                // in the replacement. The other option is to just use the new character but that will convert the
                // entire output to upper case.
                inputChars[i] = Character.isUpperCase(inputChars[i]) ? newChar : Character.toLowerCase(newChar);
            }
        }

        // If we were given an output file then we open it, write the output, flush the data and close it.
        if (outputFile != null) {
            try {
                FileOutputStream fos = new FileOutputStream(outputFile);
                fos.write(new String(inputChars).getBytes());
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // Otherwise we dump it to the console
            System.out.println(new String(inputChars));
        }
    }
}
#include <stdio.h>
#include <string.h>
#include <malloc.h>

int main(int argc, char *argv[]) {
    // argv 1 - name of file to read in a list of names
    // argv 2 - name of file to write out the sorted list of names
    if (argc == 2) {
        printf("%s\n", argv[1]);
    } else if (argc == 3) {
        printf("%s\n", argv[1]);
        printf("%s\n", argv[2]);
    }

    // Reading from the file. We need to open the file which we do by creating a file pointer to the path specified in
    // the command line parameters. We open it in read mode.
    FILE *fp = fopen(argv[1], "r");

    // If we want to store all of the names that are contained within the file, we need to define an array to hold them
    // but because arrays are of a fixed size, we need to count them first. Therefore we scan through the file and
    // count up the number of commas which are our delimiters.
    int c = 0;
    int read;
    while ((read = fgetc(fp)) != EOF) if (read == ',') c++;
    // Add one for the last entry in the file
    c++;

    // Now that we know the size, we can define the array to hold them
    char *words[c];

    //Next we want to read in the file so that we can do some processing on it. To do that we need to know how many
    // characters we need to read and to do that we need to know how long the file is.
    // To do this we can just move the file position indicator to the end of the file (this does not have ensured
    // portibility, however), get the position the move it back to the beginning of the file (which will reset the
    // moves we did above as well when scanning the file). This is not a perfect solution as it is not fully portable
    // but it seemed to work. I will look for the better way to do this later.
    fseek(fp, 0, SEEK_END);
    long size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // Then we define the memory for the file and read in the contents of the file. We use a size of one byte and read
    // size number of elements from the fp pointer.
    char *fcontent = malloc(size);
    fread(fcontent, 1, size, fp);

    // Then we close the file so we don't have handle issues later.
    fclose(fp);

    // This bit of the code is a little bit of a mess
    // We define an int to act as the index we are currently writing to in the words array, effectively the current name
    // we are on
    int i = 0;
    // We use strtok to take the first set of characters all the way up to the first , character. This will get us the
    // first name.
    char *token = strtok(fcontent, ",");
    // As the name is formatted as "NAME", we need to remove the first and last character of the string so we do this
    // by incrementing the start pointer which means the start points to one character in and set the last character to
    // a null terminator which seems to do the job
    token++;
    token[strlen(token) - 1] = '\0';
    // Then we store the name and increment the index after that is done
    words[i++] = token;

    // Next we find the next token in preparation for the next loop. We use NULL as the string here as it will cause it
    // to continue searching the string that we first specified from the end of the previous token but we use the same
    // delimiter character.
    token = strtok(NULL, ",");
    // While we have a valid token (this will be NULL when we reach the end of the file as the strtok function returns
    // NULL if there are no more valid tokens contained within the string.)
    while (token != NULL) {
        // Remove the first and last character as described above
        token++;
        token[strlen(token) - 1] = '\0';
        // Store the word in the same way described above
        words[i++] = token;
        // And find the next token
        token = strtok(NULL, ",");
    }

    // Now we move on to the actual bubble sort code
    // We define a flag for whether an element has been moved in this loop. We initially set it to false but as this is
    // a post-tested loop and we are redefining it to false in the loop, the definition is technically pointless.
    int move = 0;
    do {
        // We want to find out if something moved during this run through the name list so we set it to false. If it
        // is false by the time we reach the end, it means that we could not move anything so the entire thing was in
        // the right order and we can break out.
        move = 0;
        // Then for every name in the array (some wizardry magic to find out the number of elements, sort of. We take
        // the length of the array of arrays and we divide it by the size of one of the elements to determine how many
        // there are because its size will be equal to (count * sizeof(string)) technically but not quite because
        // strings aren't a thing. Maybe it is just magic, this is C after all)
        for (int j = 0; j < (sizeof(words) / sizeof(words[0])) - 1; ++j) {
            // Fetch the two names that we are comparing here
            char *name1 = words[j];
            char *name2 = words[j + 1];

            // Then we want to find the minimum length. Therefore we get the length of the first, then test if the
            // length of the second is shorter. If it is, that becomes the new length otherwise we will stick with the
            // first length. This will also handle if they are equal
            int length = strlen(name1);
            if (strlen(name2) < length) length = strlen(name2);

            // The comparison function is defined according to the specification laid out in the Java String#compareTo
            // function documentation. That is copied below to explain it in more detail
            // From https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#compareTo(java.lang.String)
            //   Compares two strings lexicographically. The comparison is based on the Unicode value of each character
            //   in the strings. The character sequence represented by this String object is compared lexicographically
            //   to the character sequence represented by the argument string. The result is a negative integer if this
            //   String object lexicographically precedes the argument string. The result is a positive integer if this
            //   String object lexicographically follows the argument string. The result is zero if the strings are
            //   equal; compareTo returns 0 exactly when the equals(Object) method would return true.
            //   This is the definition of lexicographic ordering. If two strings are different, then either they have
            //   different characters at some index that is a valid index for both strings, or their lengths are
            //   different, or both. If they have different characters at one or more index positions, let k be the
            //   smallest such index; then the string whose character at position k has the smaller value, as determined
            //   by using the < operator, lexicographically precedes the other string. In this case, compareTo returns
            //   the difference of the two character values at position k in the two string -- that is, the value:
            //       this.charAt(k)-anotherString.charAt(k)
            //   If there is no index position at which they differ, then the shorter string lexicographically precedes
            //   the longer string. In this case, compareTo returns the difference of the lengths of the strings -- that
            //   is, the value:
            //       this.length()-anotherString.length()
            int compare = 0;
            for (int k = 0; k < length; ++k) {
                compare = name2[k] - name1[k];
                if (compare != 0) break;
            }

            if (compare == 0) compare = strlen(name2) - strlen(name1);

            // If the second word should go before the first then we move them around and we mark that we made a move
            // during this run
            if (compare < 0) {
                words[j] = name2;
                words[j + 1] = name1;
                move = 1;
            }
        }

    } while (move);

    // To produce the output in the same format the file was in, we need to create a string that contains every name
    // wrapped in quotes, delimited by commas. We know that the content of the names will not be changed and we know
    // that we will be readding the quotes and commas that we removed earlier so it will be the same length as the
    // original so we just need to define a piece of memory that large.
    char *output = malloc(size);
    // We need to know the position in the memory that we are currently writing to so we create an index for that
    int outputCounter = 0;

    // Then for each word in the list of names (magic described above)
    for (int l = 0; l < sizeof(words) / sizeof(words[0]); ++l) {
        // All writes to the output will use the index outputCounter++ which will use the current value of outputCounter
        // but will increment it to the next value afterwards. This means that it will always point to the next
        // available space when the line completes.

        // If this isn't the first value, we add a comma. This saves us having to remove one character at the end
        if (outputCounter != 0) output[outputCounter++] = ',';
        // Then we add the first quote
        output[outputCounter++] = '\"';
        // Copy in the word character by character (there is likely a better way to do this)
        for (int j = 0; j < strlen(words[l]); ++j) {
            output[outputCounter++] = words[l][j];
        }
        // And wrap it in the other quote
        output[outputCounter++] = '\"';
    }
    // Then we finally add a null terminator to make sure the string ends
    output[outputCounter] = '\0';

    // If we gave two parameters, an infile and an outfile, we open the outfile, write the output and close it
    if (argc == 3) {
        FILE *outputFile = fopen(argv[2], "w");
        fputs(output, outputFile);
        fclose(outputFile);
    } else {
        // If we did not, then we just dump the response onto the standard output
        printf("%s\n", output);
    }

    // And we exit successfully.
    return 0;
}
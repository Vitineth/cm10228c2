import java.io.*;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Q3Main {

    public static void main(String[] args) {
        // args 0 - name of file to read in a list of names
        String inputFileName = null;
        // args 1 - name of file to write out the sorted list of names
        String outputFileName = null;
        if (args.length == 1) {
            inputFileName = args[0];
        } else if (args.length == 2) {
            inputFileName = args[0];
            outputFileName = args[1];
        }

        // call method to quick sort list of names
        new Q3Main().sortNames(inputFileName, outputFileName);
    }

    public void sortNames(String input, String output) {
        File inputFile = new File(input);
        File outputFile = null;
        if (output != null) {
            outputFile = new File(output);
            // Verify the output file and output a warning if it exists
            if (outputFile.exists()) System.err.println("Output file exists, it will be overwritten");
        }

        // Verify the input file exists
        if (!inputFile.exists()) {
            // Otherwise throw a descriptive error and exit
            try {
                throw new FileNotFoundException("Input file not found");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }

        // Read in all the content from the file
        String namesRaw;

        try {
            // This is apparently the right way to do it. You read using a fixed size buffer, in this case 1kb. Then
            // you copy the amount that was read into an byte array output stream which you can then later convert
            // to a string. Once you reach -1 you know you have copied all so it will fall out and define the full
            // content of the file
            FileInputStream fis = new FileInputStream(inputFile);
            byte[] buffer = new byte[1024];
            int len;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = fis.read(buffer)) != -1) baos.write(buffer, 0, len);

            namesRaw = baos.toString();
        } catch (IOException e) {
            System.err.println("Failed to read the input file");
            e.printStackTrace();
            return;
        }

        // Then we strip out the quotes, and divide on a comma so we have a list of names
        String[] names = namesRaw.replace("\"", "").split(",");

        // Call the quicksort on the data
        sort(names, 0, names.length - 1);

        // Once we have dropped out we know we have completed it so we stream all the names (this is a very cool feature
        // of java now) and apply the map function which will perform the same action to all elements and return a new
        // stream of all the edited elements, in this case we add quotes to either side then we collect all the new
        // values together using a joining collector which uses a comma to collect them all so it follows the format
        // used in the original file.
        String writeOutput = Arrays.stream(names).map(s -> "\"" + s + "\"").collect(Collectors.joining(","));

        // If we were given an output file then we open it, write the output, flush the data and close it.
        if (outputFile != null) {
            try {
                FileOutputStream fos = new FileOutputStream(outputFile);
                fos.write(writeOutput.getBytes());
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // Otherwise we dump it to the console
            System.out.println(writeOutput);
        }
    }

    // Quicksort algorithm implemented from Wikipedia pseudocode

    void sort(String[] names, int lowIndex, int highIndex) {
        if (lowIndex < highIndex) {
            int p = partition(names, lowIndex, highIndex);
            sort(names, lowIndex, p - 1);
            sort(names, p + 1, highIndex);
        }
    }

    int partition(String[] names, int lowIndex, int highIndex) {
        String pivot = names[highIndex];
        int i = lowIndex - 1;
        for (int j = lowIndex; j < highIndex; j++) {
            if (pivot.compareTo(names[j]) > 0) {
                i++;
                String temp = names[j];
                names[j] = names[i];
                names[i] = temp;
            }
        }
        String temp = names[i + 1];
        names[i + 1] = names[highIndex];
        names[highIndex] = temp;

        return i+1;
    }
}
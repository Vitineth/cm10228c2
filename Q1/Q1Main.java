import java.io.*;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Q1Main {

    public static void main(String[] args) {
        // args 0 - name of file to read in a list of names
        String inputFileName = null;
        // args 1 - name of file to write out the sorted list of names
        String outputFileName = null;
        if (args.length == 1) {
            inputFileName = args[0];
        } else if (args.length == 2) {
            inputFileName = args[0];
            outputFileName = args[1];
        }

        // call method to bubble sort list of names
        new Q1Main().sortNames(inputFileName, outputFileName);
    }

    public void sortNames(String input, String output) {
        File inputFile = new File(input);
        File outputFile = null;
        if (output != null) {
            outputFile = new File(output);
            // Verify the output file and output a warning if it exists
            if (outputFile.exists()) System.err.println("Output file exists, it will be overwritten");
        }

        // Verify the input file exists
        if (!inputFile.exists()) {
            // Otherwise throw a descriptive error and exit
            try {
                throw new FileNotFoundException("Input file not found");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }

        // Read in all the content from the file
        String namesRaw;

        try {
            // This is apparently the right way to do it. You read using a fixed size buffer, in this case 1kb. Then
            // you copy the amount that was read into an byte array output stream which you can then later convert
            // to a string. Once you reach -1 you know you have copied all so it will fall out and define the full
            // content of the file
            FileInputStream fis = new FileInputStream(inputFile);
            byte[] buffer = new byte[1024];
            int len;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((len = fis.read(buffer)) != -1) baos.write(buffer, 0, len);

            namesRaw = baos.toString();
        } catch (IOException e) {
            System.err.println("Failed to read the input file");
            e.printStackTrace();
            return;
        }

        // Then we strip out the quotes, and divide on a comma so we have a list of names
        String[] names = namesRaw.replace("\"", "").split(",");

        // We define a flag for whether an element has been moved in this loop.
        boolean move;
        do {
            // We want to find out if something moved during this run through the name list so we set it to false. If it
            // is false by the time we reach the end, it means that we could not move anything so the entire thing was in
            // the right order and we can break out.
            move = false;
            // Then for every name in the array
            for (int i = 0; i < names.length - 1; i++) {
                // Fetch the two names that we are comparing here
                String w1 = names[i];
                String w2 = names[i + 1];

                // Then we use the built in compare function. 0 means that they are equal, <0 means that the string
                // passed in (w1) is lexicographically smaller than the other meaning that is comes first alphabetically
                // and >0 means the opposite.
                int compared = w2.compareTo(w1);
                if (compared < 0) {
                    // So if it is less then we swap the values and make a note that we did actually move a value during
                    // this run
                    names[i] = w2;
                    names[i + 1] = w1;
                    move = true;
                }

            }

        } while (move);

        // Once we have dropped out we know we have completed it so we stream all the names (this is a very cool feature
        // of java now) and apply the map function which will perform the same action to all elements and return a new
        // stream of all the edited elements, in this case we add quotes to either side then we collect all the new
        // values together using a joining collector which uses a comma to collect them all so it follows the format
        // used in the original file.
        String writeOutput = Arrays.stream(names).map(s -> "\"" + s + "\"").collect(Collectors.joining(","));

        // If we were given an output file then we open it, write the output, flush the data and close it.
        if (outputFile != null) {
            try {
                FileOutputStream fos = new FileOutputStream(outputFile);
                fos.write(writeOutput.getBytes());
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // Otherwise we dump it to the console
            System.out.println(writeOutput);
        }
    }
}